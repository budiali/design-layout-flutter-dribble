import 'package:flutter/material.dart';
import 'package:review_dribble/presentation/app_widgets.dart';

void main() {
  runApp(const AppWidgets());
}
