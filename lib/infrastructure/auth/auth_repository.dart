import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:review_dribble/domain/auth/login_request.dart';
import 'package:review_dribble/domain/auth/login_response.dart';

class AuthRepository {
  final Dio _dio = Dio();

  Future<Either<String, LoginResponse>> signInUserWithEmailAndPassword(
      {required LoginRequest loginRequest}) async {
    Response _res;

    try {
      _res = await _dio.post('https://reqres.in/api/login',
          data: loginRequest.toJson());
      LoginResponse loginResponse = LoginResponse.fromJson(_res.data);
      return right(loginResponse);
    } on DioError catch (e) {
      // ignore: avoid_print
      print(e.response!.statusCode);
      String errorMessage = e.response!.data.toString();
      switch (e.type) {
        case DioErrorType.connectTimeout:
          break;
        case DioErrorType.sendTimeout:
          break;
        case DioErrorType.receiveTimeout:
          break;
        case DioErrorType.response:
          errorMessage = e.response!.data['error'];
          break;
        case DioErrorType.cancel:
          break;

        default:
      }
      return left(errorMessage);
    }
  }
}
