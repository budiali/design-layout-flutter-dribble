part of 'auth_cubit.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

// ignore: must_be_immutable
class AuthLoading extends AuthState {
  String msgLoading;
  AuthLoading(this.msgLoading);
}

// ignore: must_be_immutable
class AuthError extends AuthState {
  String msgError;
  AuthError(this.msgError);
}

// ignore: must_be_immutable
class AuthSuccess extends AuthState {
  LoginResponse loginResponse;
  AuthSuccess(this.loginResponse);
}
