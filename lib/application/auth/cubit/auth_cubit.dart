import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:review_dribble/domain/auth/login_request.dart';
import 'package:review_dribble/domain/auth/login_response.dart';
import 'package:review_dribble/infrastructure/auth/auth_repository.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());
  AuthRepository authRepository = AuthRepository();

  void signInUser(LoginRequest _loginRequest) async {
    emit(AuthLoading('Loading'));
    try {
      // ignore: unused_local_variable
      final _data = await authRepository.signInUserWithEmailAndPassword(
          loginRequest: _loginRequest);
      // emit(AuthSuccess(_data.));
      _data.fold((l) => emit(AuthError(l)), (r) => emit(AuthSuccess(r)));
    } catch (e) {
      emit(AuthError(e.toString()));
    }
  }
}
