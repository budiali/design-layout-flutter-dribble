import 'package:flutter/material.dart';
import 'package:review_dribble/presentation/login/login_page.dart';

class AppWidgets extends StatelessWidget {
  const AppWidgets({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginTwelvePage(),
    );
  }
}
