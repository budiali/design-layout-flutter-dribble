import 'package:flutter/material.dart';
import 'package:review_dribble/presentation/profil/widgets/card_profile.dart';
import 'package:review_dribble/presentation/profil/widgets/title_and_subtitle_widget.dart';
import 'widgets/discover_list_item_widget.dart';
import 'widgets/featured_article_banner_widget.dart';

class ProfilPage extends StatefulWidget {
  const ProfilPage({Key? key}) : super(key: key);

  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.black26,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const CardProfile(),
              Container(
                  margin: const EdgeInsets.only(
                    left: 15,
                  ),
                  child: natureDiscoverData()),
              const SizedBox(
                height: 15,
              ),
              Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: const FeaturedArticleBannerWidget()),
            ],
          ),
        ),
      ),
    );
  }

  Widget natureDiscoverData() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const TitleAndSubtitleWidget(
          title: 'Notable Works!',
          subTitle: 'Based on the popularity of Articles',
        ),
        const SizedBox(
          height: 15,
        ),
        SizedBox(
          height: 200,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 20,
              itemBuilder: (context, index) {
                return const DiscoverListItemWidget();
              }),
        )
      ],
    );
  }
}
