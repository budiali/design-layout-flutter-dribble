import 'package:flutter/material.dart';

class TitleAndSubtitleWidget extends StatelessWidget {
  final String title;
  final String subTitle;

  const TitleAndSubtitleWidget(
      {Key? key, required this.title, required this.subTitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black87),
        ),
        const SizedBox(
          height: 8,
        ),
        Text(
          subTitle,
          style: const TextStyle(
              fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black38),
        ),
      ],
    );
  }
}
