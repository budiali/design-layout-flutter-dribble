import 'package:flutter/material.dart';
import 'package:review_dribble/presentation/profil/widgets/information_profile.dart';

class CardProfile extends StatelessWidget {
  const CardProfile({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.all(10),
      height: 250,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), color: Colors.white),
      child: Column(
        children: [
          SizedBox(
            // color: Colors.blueAccent,
            height: 170,
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  width: 140,
                  decoration: BoxDecoration(
                      image: const DecorationImage(
                        image: AssetImage('assets/images/budi.jpg'),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(25)),
                ),
                Flexible(
                  child: Container(
                    margin: const EdgeInsets.only(left: 20, top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // ignore: prefer_const_constructors
                        Text(
                          'Budi Ali',
                          // ignore: prefer_const_constructors
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          'Full Stack Developer',
                          style: TextStyle(fontSize: 15, color: Colors.black54),
                          overflow: TextOverflow.ellipsis,
                        ),
                        Flexible(
                            child: Container(
                          margin: const EdgeInsets.only(top: 30),
                          height: 60,
                          decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.circular(8)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: const [
                              InformationProfile(
                                  title: 'Articles', subtitle: '43'),
                              InformationProfile(
                                  title: 'Followers', subtitle: '8.936'),
                              InformationProfile(
                                  title: 'Rating', subtitle: '82'),
                            ],
                          ),
                        ))
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Flexible(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                flex: 1,
                child: OutlinedButton(
                  onPressed: () {},
                  child: const Text('Chat'),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 1,
                child: ElevatedButton(
                  onPressed: () {},
                  child: const Text('Follow'),
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                  )),
                ),
              )
            ],
          ))
        ],
      ),
    );
  }
}
